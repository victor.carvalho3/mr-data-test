# Desafio Técnico: coletando e transformando dados do CDI

## O que é o CDI?

[https://maisretorno.com/portal/termos/c/cdi](https://maisretorno.com/portal/termos/c/cdi)

## Apresentação do Problema

O desafio consiste na coleta e transformação do histórico de um índice vastamente utilizado no mercado financeiro: o [CDI](https://maisretorno.com/portal/termos/c/cdi). Neste contexto, deve-se criar um *pipeline* que faça as buscas em uma fonte específica e gere o **histórico de taxa (rentabilidade) acumulada de tal índice**.

Como fonte de dados, vamos utilizar a calculadora da B3 disponibilizada em [https://calculadorarendafixa.com.br/#](https://calculadorarendafixa.com.br/#). Na parte inferior da página, encontra-se a ferramenta para realização de alguns testes. 

![image1](images/image1.png)

No exemplo acima, fez-se uma requisição para obter o rendimento de 100% da taxa DI considerando o período entre **02/01/2021** até **21/07/2021.** Como resultado, obtivemos alguns campos importantes como **Fator, Taxa, Valor Base e Valor Calculado.** Segue explicação:

- Fator: termo de multiplicação para obtenção do valor final do rendimento
- Taxa: valor, em porcentagem, referente à rentabilidade no período requisitado. Note que a taxa é obtida como:

```math
Taxa = 100 \times (Fator - 1)
```

- Valor Base: valor referente ao que foi colocado no campo **Valor** da consulta. Refere-se ao montante inicial que seria colocado como investimento.
- Valor Calculado: montante na data final. Note que tal valor é calculado como:

```math
Valor Calculado = Valor Base \times Fator
```

Dessa forma, para obtenção do histórico de rentabilidade diária desde o início, deve-se realizar diversas consultas com diferença de um dia entre **Data Inicial** e **Data Final mantendo campo Percentual  em** **100,00**. Por exemplo,

![image2](images/image2.png)

Neste exemplo acima, tem-se que a rentabilidade do CDI foi de aproximadamente 0,01% e, caso tivéssemos aplicado R$ 1.000,00 no dia 05/01/2021, teríamos R$ 1.000,07 no dia 06/01/2021.

Todavia, note que, neste desafio, deve-se obter a **taxa acumulada** desde o início do histórico disponível, ou seja, 04/07/1994. Para isso, deve-se **coletar os fatores diários** e **depois realizar  um produto acumulado entre eles**. Por exemplo,

|    Data    | Fator | Fator Acumulado |
|:----------:|:-----:|:---------------:|
| 01/04/2021 |   -   |        -        |
| 02/04/2021 |  1,07 |       1,07      |
| 03/04/2021 |  1,07 |      1,1449     |
| 04/04/2021 |  1,07 |     1,225043    |

Assim, a taxa acumulada será simplesmente fazer o cálculo pela equação apresentada anteriormente. Como resultado final, teríamos:

|    Data    | Fator | Fator Acumulado | Taxa Acumulada |
|:----------:|:-----:|:---------------:|:--------------:|
| 01/04/2021 |   -   |        -        |        -       |
| 02/04/2021 |  1,07 |       1,07      |       7%       |
| 03/04/2021 |  1,07 |      1,1449     |     14,49%     |
| 04/04/2021 |  1,07 |     1,225043    |    25,5043%    |

## Informações Úteis

- Usar a seguinte URL de requisição para a calculadora:

    ```python
    https://calculadorarendafixa.com.br/calculadora/di/calculo?dataInicio=DATA_INICIAL&dataFim=DATA_FINAL&percentual=100.00&valor=1000.00
    ```

    **DATA_INICIAL:** string de data no formato 2021-01-01 (YYYY-mm-dd)

    **DATA_FINAL:** string de data no formato 2021-01-01 (YYYY-mm-dd)

    EXEMPLO DE RESPOSTA:

    ```json
     {"dataInicial":"01/01/2020","dataFinal":"01/01/2021","percentual":"100.00","fator":"1.02757816","taxa":"2.76","valorBase":"1000.00","valorCalculado":"1027.57"}
    ```

- Existe um docker com cluster Spark e Jupyter Notebook para desenvolvimento da solução ([https://github.com/cluster-apps-on-docker/spark-standalone-cluster-on-docker](https://github.com/cluster-apps-on-docker/spark-standalone-cluster-on-docker)). O uso é opcional.

## Requisitos

- A saída do processo deve ser uma **tabela** **delta** ([https://delta.io/](https://delta.io/)) e ter estrutura conforme mostrada na apresentação do problema;
- Pode-se utilizar outras bibliotecas que sejam da vontade do desenvolvedor para realizar as requisições e manipulações necessárias. Todavia, o Spark deve ser utilizado em alguma etapa do processo (quanto mais usado, melhor avaliada será a solução);
- Não há restrições de linguagem, mas sugere-se o uso de Python ou Scala pela compatibilidade com Spark;
- Deve-se utilizar as versões 2 ou 3 do Spark;
- Entregáveis: código-fonte e arquivos de saída do processo;
- Prazo de entrega: 5 dias.

## **Pontos de Avaliação**

- Qualidade do código;
- Adequação da solução;
- Tempo de entrega;
- Criatividade (vamos verificar se o candidato fez algo a mais além do solicitado).
